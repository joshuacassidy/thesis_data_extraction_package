from keras.datasets import mnist
import os
import extract_utils

(x_train, y_train), (x_test, y_test) = mnist.load_data()

mnist_label_names = {
    0: "0", 
    1: "1",
    2: "2",
    3: "3",
    4: "4",
    5: "5",
    6: "6",
    7: "7",
    8: "8",
    9: "9",
}

output_dir = 'mnist_unstructured'

extract_utils.create_unstructured_directory(mnist_label_names, output_dir, x_train, y_train, x_test, y_test)

extract_utils.create_structured_directory('mnist')

dataset_source_directory = 'mnist_unstructured'
labels = os.listdir(dataset_source_directory)

extract_utils.populate_directory(
    dataset_source_directory=dataset_source_directory, 
    output_dir='mnist', 
    directory_type='test', 
    start_image_amount=0,
    images_amount=1000
)

extract_utils.populate_directory(
    dataset_source_directory=dataset_source_directory, 
    output_dir='mnist', 
    directory_type='validation', 
    start_image_amount=1000,
)

extract_utils.populate_directory(
    dataset_source_directory=dataset_source_directory, 
    output_dir='mnist', 
    directory_type='train', 
    start_image_amount=1000,
)



