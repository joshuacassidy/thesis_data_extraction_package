from keras.datasets import cifar100
import os
import extract_utils

(x_train, y_train), (x_test, y_test) = cifar100.load_data()

cifar100_label_names = {0: 'apples', 1: 'aquarium fish', 2: 'baby', 3: 'bear', 4: 'beaver', 5: 'bed', 6: 'bee', 7: 'beetle', 8: 'bicycle', 9: 'bottles', 10: 'bowls', 11: 'boy', 12: 'bridge', 13: 'bus', 14: 'butterfly', 15: 'camel', 16: 'cans', 17: 'castle', 18: 'caterpillar', 19: 'cattle', 20: 'chair', 21: 'chimpanzee', 22: 'clock', 23: 'cloud', 24: 'cockroach', 25: 'couch', 26: 'crab', 27: 'crocodile', 28: 'cups', 29: 'dinosaur', 30: 'dolphin', 31: 'elephant', 32: 'flatfish', 33: 'forest', 34: 'fox', 35: 'girl', 36: 'hamster', 37: 'house', 38: 'kangaroo', 39: 'computer keyboard', 40: 'lamp', 41: 'lawn-mower', 42: 'leopard', 43: 'lion', 44: 'lizard', 45: 'lobster', 46: 'man', 47: 'maple', 48: 'motorcycle', 49: 'mountain', 50: 'mouse', 51: 'mushrooms', 52: 'oak', 53: 'oranges', 54: 'orchids', 55: 'otter', 56: 'palm', 57: 'pears', 58: 'pickup truck', 59: 'pine', 60: 'plain', 61: 'plates', 62: 'poppies', 63: 'porcupine', 64: 'possum', 65: 'rabbit', 66: 'raccoon', 67: 'ray', 68: 'road', 69: 'rocket', 70: 'roses', 71: 'sea', 72: 'seal', 73: 'shark', 74: 'shrew', 75: 'skunk', 76: 'skyscraper', 77: 'snail', 78: 'snake', 79: 'spider', 80: 'squirrel', 81: 'streetcar', 82: 'sunflowers', 83: 'sweet peppers', 84: 'table', 85: 'tank', 86: 'telephone', 87: 'television', 88: 'tiger', 89: 'tractor', 90: 'train', 91: 'trout', 92: 'tulips', 93: 'turtle', 94: 'wardrobe', 95: 'whale', 96: 'willow', 97: 'wolf', 98: 'woman', 99: 'worm'}

output_dir = 'cifar100_unstructured'

extract_utils.create_unstructured_directory(cifar100_label_names, output_dir, x_train, y_train, x_test, y_test)

extract_utils.create_structured_directory('cifar100')

dataset_source_directory = 'cifar100_unstructured'
labels = os.listdir(dataset_source_directory)

extract_utils.populate_directory(
    dataset_source_directory=dataset_source_directory, 
    output_dir='cifar100', 
    directory_type='test', 
    start_image_amount=0,
    images_amount=100
)

extract_utils.populate_directory(
    dataset_source_directory=dataset_source_directory, 
    output_dir='cifar100', 
    directory_type='validation', 
    start_image_amount=100,
)

extract_utils.populate_directory(
    dataset_source_directory=dataset_source_directory, 
    output_dir='cifar100', 
    directory_type='train', 
    start_image_amount=100,
)



